## JiHu Team
The JiHu org and point of contacts are listed below.

| Function                                | Primary Contact                         | Primary Contact GitLab Handle           | Supporting Contact Name                 | Supporting Contact GitLab Handle        |
|-----------------------------------------|-----------------------------------------|-----------------------------------------|-----------------------------------------|-----------------------------------------|
| General                                   | Sam Chen                                | @usultrared                             | Lin Cong | @lcong0715                                        
| Sales                                   | Sam Chen                                | @usultrared                             |                                         |                                         |
| Sales Ops                               | Shawn Li                                | @Shawnli                             |                                         |                                         |
| Presales                                | Gavin Wang                              | @gavinwang                              |  Xiao Gang                                       | @xiaogang_cn                                        |
| Marketing             | Louise Luo                              | @louiseluo                              |                                         |                                         |
| Engineering                          | Qian Zhang                              | @qianzhangxa                            | Fu Zhang | @prajnamas |
| CTO Office                                    | Ben Lin                                 | @ben_lin                                | Wayne Liu, XuDong Guo                              | @wayneliu0019, @sunny0826                                        |
| [Product](https://gitlab.com/gitlab-jh/jh-team/requirement-management)                  | Peng Liang                               | @lpeng1991                           |      
| [Design](https://gitlab.com/gitlab-jh/jh-team/jihu-design)                  | Yongfan Shen                               | @youngbeomshin                           |                                   |                                         |
| Customer Success and Support            | RJ Dong                                 | @ruijie.dong                            | Chris Xu                                | @nicelife8407
| Community            | Becky Zhang                                 |                             |      
| Education            | Yue Chen                                 |                             | Ma Sai                                | 
| Partner / Ecosystem            | Ma Sai                                 |                             | Zhiqi Gao                                | @gaozq
| [IT](https://gitlab.com/gitlab-jh/jh-team/it-service)                  | Yake Zhang                               | @seraph.zheng                           |                                         |                              |
| Finance                  | Shirley Pu                               |                            | Shijia Gu                                       |                              |
| Legal                  | Phyllis Yang                               |                            |                                         |                              |
| Human Resources and Recruiting          | Raymond Wang                            | @RaymondWangBJ                          | Nancy Yuan, Jing Cai                    | @Nancyjihu, @jcai1                  |
